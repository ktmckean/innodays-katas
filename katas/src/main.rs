fn main() {
    println!("Hello, world!");
}

fn is_prime(num : i64) -> bool
{
    for i in 2..num{
        if num%i == 0
        {
            return false;
        }
    }    
    return true;
}

// num must be greater than 1
fn get_next_prime_factor(num : i64) -> i64
{
    for i in 2..=num{
        if !is_prime(i) {continue};

        if num%i == 0
        {
            return i;
        }
    }
    return 0;   
}

fn prime_factors(mut num : i64) -> Vec<i64>
{
    let mut factors = Vec::new();

    while num > 1
    {
        let factor = get_next_prime_factor(num);
        if factor == 0
        {
            return factors;
        }

        factors.push(factor);
        num /= factor;
    }

    return factors;
}



#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn testZero(){
        assert_eq!(Vec::<i64>::new(), prime_factors(-1));
    }

    #[test]
    fn testMinusFive(){
        assert_eq!(Vec::<i64>::new(), prime_factors(-5));
    }

    #[test]
    fn testOne(){
        assert_eq!(Vec::<i64>::new(), prime_factors(1));
    }

    #[test]
    fn testTwo(){
        assert_eq!(vec![2], prime_factors(2));
    }

    #[test]
    fn testThree(){
        assert_eq!(vec![3], prime_factors(3));
    }

    #[test]
    fn testSix(){
        assert_eq!(vec![2,3], prime_factors(6));
    }    

    #[test]
    fn testNine(){
        assert_eq!(vec![3,3], prime_factors(9));
    }    

    #[test]
    fn testTwenty(){
        assert_eq!(vec![2,2,5], prime_factors(20));
    } 

    #[test]
    fn testOneHundredOne(){
        assert_eq!(vec![101], prime_factors(101));
    }    
}